﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="about-us.aspx.vb" Inherits="about_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="Advance_CSS/css/bootstrap.css" rel="stylesheet" />
    <link href="Advance_CSS/css/styles.css" rel="stylesheet" />
    
     <div class="theme-hero-area theme-hero-area-half">
        <div class="theme-hero-area-bg-wrap">
    
            <div class="theme-hero-area-bg" style="background-image: url(Advance_CSS/img/brands/pexels-photo-879824.jpeg);"></div>
            <div class="theme-hero-area-mask theme-hero-area-mask-half"></div>
            <div class="theme-hero-area-inner-shadow"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 theme-page-header-abs">
                        <div class="theme-page-header theme-page-header-lg">
                            <h1 class="theme-page-header-title">DiscoverFaremonster</h1>
                            <p class="theme-page-header-subtitle">The Story of Our Company</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="theme-page-section theme-page-section-xl theme-page-section-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="theme-about-us">
                        <div class="theme-about-us-section sticky-parent">
                            <div class="row row-col-static row-col-reverse">
                                <div class="col-md-4">
                                    <div class="sticky-cols">
                                        <h4 class="theme-about-us-section-title">What We Do</h4>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="theme-about-us-section-body">



                                        <p>Fare Monster Travels Pvt. Ltd. is one of the leading travel companies in India and holds a very important place in the travel trade arena. It was founded in the year 2011 by professionals with a vast experience in the travel industry and later registered as private limited firm in 2019. It is active across varied travel segments which includes leisure and business travel. Fare Monster group has aggressively grown its corporate business and strengthened its international market position over the years. The company works closely with airline, hotel and ancillary travel service providers. As an IATA certified company, it is ensured at Fare Monster that high levels of corporate governance and standardized systems and processes are followed.</p>
                                        <p>The portfolio of clients includes a broad selection of multi-national, national, regional and local companies across every industry and specialization. The company understands that small and large businesses operate differently and it has the expertise of handling industry and volume specific requirements. Our on-ground presence across major cities enables it to provide seamless travel services which are ‘tailor-made’ to suit your business requirements.</p>
                                        <p>Fare Monster Travel Pvt. Ltd. look forward to taking you through amazing corporate travel experiences!</p>
                                        <div class="theme-about-us-section-gallery">
                                            <div class="row" data-gutter="10">
                                                <div class="col-xs-12">
                                                    <div class="banner theme-about-us-section-gallery-img theme-about-us-section-gallery-img-lg banner-">
                                                        <div class="banner-bg" style="background-image: url(Advance_CSS/img/brands/pexels-photo-59519.jpeg);"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="banner theme-about-us-section-gallery-img theme-about-us-section-gallery-img-lg banner-">
                                                        <div class="banner-bg" style="background-image: url(Advance_CSS/img/brands/pexels-photo-386009.jpeg);"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="banner theme-about-us-section-gallery-img theme-about-us-section-gallery-img-lg banner-">
                                                        <div class="banner-bg" style="background-image: url(Advance_CSS/img/brands/pexels-photo-879824.jpeg);"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="theme-about-us-section sticky-parent">
                            <div class="row row-col-static row-col-reverse">
                                <div class="col-md-4">
                                    <div class="sticky-cols">
                                        <h4 class="theme-about-us-section-title">Our History</h4>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="theme-about-us-section-body">
                                        <p>Faremonster.com is a new-fangled traveling portal managed by a team of immensely experienced people in the B2B Market. www.hotelexpertz.com is one of our leading extravagant Hotel Search Engine Portal successfully in operation since Last 3 years in India?s Domestic/International travel industry.</p>
                                        <p>Already a renowned brand in travel industry, Faremonster Pvt. Ltd. an IATA certified company which is another subsidiary of Faremonster.com. Collectively Faremonster.com has become travellers delight with out-of-the-box featured services for all travel needs.</p>
                                        <div class="theme-about-us-section-gallery">
                                            <div class="row" data-gutter="10">
                                                <div class="col-xs-8">
                                                    <div class="banner theme-about-us-section-gallery-img banner-">
                                                        <div class="banner-bg" style="background-image: url(Advance_CSS/Images/adult-amsterdam-chairs-comfortable-509819_370x165.jpg);"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <div class="banner theme-about-us-section-gallery-img banner-">
                                                        <div class="banner-bg" style="background-image: url(Advance_CSS/Images/colleagues-cooperation-fist-bump-fists-398532_180x165.jpg);"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <div class="banner theme-about-us-section-gallery-img banner-">
                                                        <div class="banner-bg" style="background-image: url(Advance_CSS/Images/adult-adventure-ancient-architecture-346826_180x165.jpg);"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <div class="banner theme-about-us-section-gallery-img banner-">
                                                        <div class="banner-bg" style="background-image: url(Advance_CSS/Images/man-in-black-jacket-holding-a-black-hardtail-bike-near-black-and-white-art-wall-173301_180x165.jpg);"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <div class="banner theme-about-us-section-gallery-img banner-">
                                                        <div class="banner-bg" style="background-image: url(Advance_CSS/Images/directly-above-shot-of-keyboard-on-table-317383_180x165.jpg);"></div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="banner theme-about-us-section-gallery-img theme-about-us-section-gallery-img-lg banner-">
                                                        <div class="banner-bg" style="background-image: url(Advance_CSS/Images/accommodation-adult-amsterdam-billiards-509780_555x200.jpg);"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

