﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="true" CodeFile="Testimonials.aspx.cs" Inherits="Testimonials" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="theme-hero-area theme-hero-area-half">
        <div class="theme-hero-area-bg-wrap">
            <div class="theme-hero-area-bg" style="background-image: url(https://10000startups.com/photos/1/testimonials-new1.jpg);"></div>
            <div class="theme-hero-area-mask theme-hero-area-mask-half"></div>
            <div class="theme-hero-area-inner-shadow"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 theme-page-header-abs">
                        <div class="theme-page-header theme-page-header-lg">
                            <h1 class="theme-page-header-title">Testimonials</h1>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="theme-page-section theme-page-section-xl theme-page-section-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="theme-payment-success">

                        <div class="theme-payment-success-box">

                            <p>“Based on my friend's very positive reviews, we booked a 7 day tour with them and also, used them to arrange our car transport from Delhi to Dubai.</p>
                            <p>I found the service and response of this travel agency to be excellent. Easily 5-stars. My emails were promptly responded to, and everyone showed up when they were supposed to.</p>
                            <p>The only reason I'm giving this travel agency 5-stars is because our tour was very good and over-the-top fantastic, so there is very little room for improvement (or, for travelers, some reason to consider other options). While a nice tour, we found the English of our private guide was excellent and the tour was somewhat "more than just a tour its a lovely memory." Like we had lunch in typical but unique not very good tourist trap restaurant. He did take us a little "off the beaten path" at the sites, however, so it was a very enjoyable tour. Both the guide and driver were friendly and courteous, and they constantly plied us with free bottled water (and you're going to need that, even in the "cooler" winter months). The 5 am sunrise is a bit ridiculous these days (my favorite shots are of the bugs and hundreds of other tourists snapping photos), but I'd do it again "for the experience.</p>
                            <p>It's a five thumbs up for this amazing, cost friendly and supportive travel agency”</p>
                            <p></p>
                            <br />
                            <p style="font-weight: 600;">Ms. Purnima,</p>
                            <p style="font-weight: 600;">Credit officer,</p>
                            <p style="font-weight: 600;">Bank of India</p>
                        </div>

                    </div>
                </div>

                <div class="col-md-6">
                    <div class="theme-payment-success">

                        <div class="theme-payment-success-box">

                            <p>“Hi Fare Monster and Team,</p>
                            <p>Through this email I want to share my experience of Goa Holiday booking with your company.</p>
                            <p>I recently went to Goa in December’20 using your services. All the arrangements of Hotels, internal transfers were just superb.. We did not have to wait for any service and everything was meticulously planned.</p>
                            <p>Thank you so much for this experience. I shall certainly endorse you to more of my friends.”</p>
                            
                            <br />

                            <p style="font-weight: 600;">Mahendra Kumar,</p>
                            <p style="font-weight: 600;">Zonal manager, LIC of India</p>



                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

