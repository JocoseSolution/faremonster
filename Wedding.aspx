﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForHome.master" AutoEventWireup="true" CodeFile="Wedding.aspx.cs" Inherits="Wedding" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style type="text/css">
        .theme-footer-section-list > li > a {
    font-size: 20px;
    padding-bottom: 30px;
    font-weight: 600;
    color: #000;
    display: block;
     opacity: 1 !important;  
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=40)";
    filter: alpha(opacity=40);
    -webkit-transition: 0.3s;
    -moz-transition: 0.3s;
    -o-transition: 0.3s;
    -ms-transition: 0.3s;
    transition: 0.3s;
}


        .theme-page-section-subtitle {
    margin-bottom: 49px;
    margin-top: 53px;
    font-size: 17px;
    opacity: 1 !important;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=45)";
    filter: alpha(opacity=45);
}
p {
    margin: 0 0 10px;
}
    </style>

    <div class="theme-page-section theme-page-section-xxl theme-page-section-gray">
      <div class="container">

             <div class="theme-page-section-header" style="text-align:left;">
          <h5 class="theme-page-section-title">Wedding</h5>
          <p class="theme-page-section-subtitle">We are Fare Monster Travels Pvt. Ltd., one of the leading travel and event planning companies in India. It was founded in the year 2011 by professionals with a vast experience in the travel industry and later registered as private limited firm in 2019. As an IATA certified company, it is ensured at Fare Monster that high levels of corporate governance and standardized systems and processes are followed.</p>
            <p class="theme-page-section-subtitle">We put our expertise in event & travel management for wedding planning which allows you to relax and enjoy your wedding while keeping all the expenditure within the permissible budget. We're here for you in every step involved in the planning and execution of your wedding. Someone who speaks your language, understands your style with a calm & collective demeanors to execute your wedding plans seamlessly with lots of excited hugs along the way!</p>
            <p class="theme-page-section-subtitle">From our initial call, right through to the emotional farewells; we're here to guide, source & coordinate every aspect of your wedding celebrations:</p>
        </div>


        <div class="row" data-gutter="60">
          <div class="col-md-4 ">
            <div class="theme-hero-text _mb-mob-15">
             <ul class="theme-footer-section-list">
                    <li>
                      <a href="#">⪢	Venue Search</a>
                    </li>
                    <li>
                      <a href="#">⪢	Full Planning</a>
                    </li>
                    <li>
                      <a href="#">⪢	Partial Planning</a>
                    </li>
                    <li>
                      <a href="#">⪢	Photography</a>
                    </li>
                    <li>
                      <a href="#">⪢	Hospitality & Guest Management </a>
                    </li>
                    <li>
                      <a href="#">⪢	Wedding Logistics</a>
                    </li>
                
                  </ul>
            </div>
          </div>
          <div class="col-md-8 ">
            <div class="row magnific-gallery row-col-gap" data-gutter="10">
              <div class="col-xs-3 ">
                <div class="banner _br-2 banner-sqr banner-">
                  <div class="banner-bg" style="background-image:url(Advance_CSS/img/brands/pexels-photo-169198.jpeg);"></div>
                  <a class="banner-link" href="Advance_CSS/img/brands/pexels-photo-169198.jpeg"></a>
                </div>
              </div>
              <div class="col-xs-3 ">
                <div class="banner _br-2 banner-sqr banner-">
                  <div class="banner-bg" style="background-image:url(Advance_CSS/img/brands/pexels-photo-169190.jpeg);"></div>
                  <a class="banner-link" href="Advance_CSS/img/brands/pexels-photo-169190.jpeg"></a>
                </div>
              </div>
              <div class="col-xs-3 ">
                <div class="banner _br-2 banner-sqr banner-">
                  <div class="banner-bg" style="background-image:url(Advance_CSS/img/brands/son-in-law-cufflinks-black-and-white-bow-tie-38270.jpeg);"></div>
                  <a class="banner-link" href="Advance_CSS/img/brands/son-in-law-cufflinks-black-and-white-bow-tie-38270.jpeg"></a>
                </div>
              </div>
              <div class="col-xs-3 ">
                <div class="banner _br-2 banner-sqr banner-">
                  <div class="banner-bg" style="background-image:url(https://images.pexels.com/photos/169196/pexels-photo-169196.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500);"></div>
                  <a class="banner-link" href="https://images.pexels.com/photos/169196/pexels-photo-169196.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"></a>
                </div>
              </div>
              <div class="col-xs-3 ">
                <div class="banner _br-2 banner-sqr banner-">
                  <div class="banner-bg" style="background-image:url(https://www.itl.cat/pngfile/big/293-2930560_photo-wallpaper-wallpaper-love-bokeh-hands-wedding-iphone.jpg);"></div>
                  <a class="banner-link" href="https://www.itl.cat/pngfile/big/293-2930560_photo-wallpaper-wallpaper-love-bokeh-hands-wedding-iphone.jpg"></a>
                </div>
              </div>
              <div class="col-xs-3 ">
                <div class="banner _br-2 banner-sqr banner-">
                  <div class="banner-bg" style="background-image:url(https://images.pexels.com/photos/818649/pexels-photo-818649.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500);"></div>
                  <a class="banner-link" href="https://images.pexels.com/photos/818649/pexels-photo-818649.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"></a>
                </div>
              </div>
              <div class="col-xs-3 ">
                <div class="banner _br-2 banner-sqr banner-">
                  <div class="banner-bg" style="background-image:url(https://i.pinimg.com/originals/a7/05/76/a705761db46db106fe27fb25f1f2450e.jpg);"></div>
                  <a class="banner-link" href="https://i.pinimg.com/originals/a7/05/76/a705761db46db106fe27fb25f1f2450e.jpg"></a>
                </div>
              </div>
              <div class="col-xs-3 ">
                <div class="banner _br-2 banner-sqr banner-">
                  <div class="banner-bg" style="background-image:url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRNCXrVRHIyDIZrmph5lD8azc-b4C7XOeXWjA&usqp=CAU);"></div>
                  <a class="banner-link" href="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRNCXrVRHIyDIZrmph5lD8azc-b4C7XOeXWjA&usqp=CAU"></a>
                </div>
              </div>
            </div>
          </div>
        </div>
          <br />
          <br />

          <div class="row">
              <p>We look forward to taking you through amazing experiences on your special day!</p>
          </div>
      </div>
    </div>


</asp:Content>

