﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="true" CodeFile="Bankdetails.aspx.cs" Inherits="SprReports_Accounts_Bankdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <br />
    <br />
    <section class="">

        
        
		<div class="container">
         
			<div class="row">
				



                <div class="col-md-5 col-sm-5">
                                            
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="https://www.searchpng.com/wp-content/uploads/2019/01/ICICI-Bank-PNG-Icon.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">ICICI Bank</h4>
                            <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	FARE MONSTER TRAVELS PVT LTD<br />BANK NAME        :	ICICI BANK LTD<br />ACCOUNT NO       :	425405000243<br />IFSC code        :			ICIC0004254</p>
						</div>
					</div>
                     
				</div>


              
                <div class="col-md-5 col-sm-5">
                                            
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="https://logos-download.com/wp-content/uploads/2016/12/Axis_Bank_logo_logotype.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">AXIS Bank</h4>
                            <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	FARE MONSTER TRAVELS PVT LTD<br />BANK NAME        :	AXIS BANK<br />ACCOUNT NO       :	921020006442778<br />IFSC code        :			UTIB0003893</p>
						</div>
					</div>
                     
				</div>
				
				<div class="col-md-5 col-sm-5">
                                            
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="https://www.freepnglogos.com/uploads/sbi-logo-png/sbi-logo-state-bank-india-group-vector-eps-0.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">STATE BANK OF INDIA</h4>
                            <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BENEFICIARY NAME :	FARE MONSTER TRAVELS PVT LTD<br />BANK NAME        :	SBI<br />ACCOUNT NO       :	39470957497<br />IFSC code        :			SBIN0011563</p>
						</div>
					</div>
                     
				</div>
			


            
		</div>

      
     <br />
     <br />
     <br />
        

	</section>



    

</asp:Content>

