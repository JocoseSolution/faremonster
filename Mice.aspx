﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForHome.master" AutoEventWireup="true" CodeFile="Mice.aspx.cs" Inherits="Mice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="theme-page-section theme-page-section-xxl">
      <div class="container">
        <div class="theme-page-section-header" style="text-align:left;">
          <h5 class="theme-page-section-title">MICE</h5>
          <p class="theme-page-section-subtitle">MICE in India has experienced tremendous growth over the past decade, and is one of the leading market segments in the travel industry. India is among the top 10 largest business travel market in the world with nearly 1.6 million Indians travelling only for the purpose of Meetings, Incentives, Conferences or Events. </p>
            <p class="theme-page-section-subtitle">Corporate travel programs can achieve a lot more than one can think. Be it any employee or a business agent, there are a few things like salary, incentives and commissions that drive one to perform at his peak. But no matter how hard you try to motivate your team, incentive travel plays a major role in inspiring the workforce. That’s where you can count on us.</p>
            <p class="theme-page-section-subtitle">FMT - MICE, a specialized offering from Fare Monster Travels Pvt. Ltd., brings global experiences for all travel needs within the Indian market. Plan your Meetings, Incentive Travel, Conferences, Events, Weddings and Team Building Activities with our diverse offerings, where at the end of every corner you get a unique and personalized experience. We offer travel services that create a lasting impression for corporate events. It is a one stop shop for all MICE related services and is one of the strongest travel solution providers in India. It’s customer focused strategies and network have consistently ensured it’s market leadership in MICE segment of the outbound and domestic travel arena.</p>
            <p class="theme-page-section-subtitle">At Fare Monster Travels Pvt. Ltd., it is our constant endeavour to accomplish more from MICE programs.</p>
        </div>
        <div class="row row-col-gap" data-gutter="10">
          <div class="col-md-4 ">
            <div class="banner _h-40vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url(Advance_CSS/img/brands/pexels-photo-787961.jpeg);"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
           
            </div>
          </div>
          <div class="col-md-8 ">
            <div class="banner _h-40vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url(Advance_CSS/img/brands/pexels-photo-567633.jpeg);"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
             
            </div>
          </div>
          <div class="col-md-3 ">
            <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url(Advance_CSS/img/brands/photo-of-people-holding-each-other-s-hands-3184423.jpg);"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
            
            </div>
          </div>
          <div class="col-md-3 ">
            <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url(Advance_CSS/img/man-couch-working-laptop-7066_180x165.jpg);"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
             
            </div>
          </div>
          <div class="col-md-3 ">
            <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url(Advance_CSS/img/bonding-cold-cozy-dog-374845_800x600.jpg);"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
             
            </div>
          </div>
          <div class="col-md-3 ">
            <div class="banner _h-33vh _br-3 banner-animate banner-animate-mask-in banner-animate-very-slow banner-animate-zoom-in">
              <div class="banner-bg" style="background-image:url(Advance_CSS/img/art-business-cactus-chair-265101_280x205.jpg);"></div>
              <div class="banner-mask"></div>
              <a class="banner-link" href="#"></a>
            
            </div>
          </div>
        </div>
      </div>
    </div>

</asp:Content>

