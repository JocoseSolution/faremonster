﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="contact-us.aspx.vb" Inherits="contact_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
<div class="theme-hero-area _h-50vh _h-mob-80vh theme-hero-area-sm">
      <div class="theme-hero-area-bg-wrap">
        <div class="theme-hero-area-bg" style="background-image:url(Advance_CSS/img/man-wearing-red-while-sitting-inside-concrete-bulding-7065_1500x800.jpg);"></div>
        <div class="theme-hero-area-mask theme-hero-area-mask-half"></div>
        <div class="theme-hero-area-inner-shadow"></div>
      </div>
      <div class="theme-hero-area-body">
        <div class="container">
          <div class="row">
            <div class="col-md-8 col-md-offset-2 theme-page-header-abs">
              <div class="theme-page-header theme-page-header-lg">
                <h1 class="theme-page-header-title">Contact us</h1>
                <div class="theme-contact-info">
                  <p>India's leading tour and travels Booking website. Book flight tickets and enjoy your holidays with distinctive experience</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <section>
		<div class="form form-spac rows con-page">
			<div class="container">
				<!-- TITLE & DESCRIPTION -->
	

		<div class="pg-contact">
                        <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con1">
                            <h2>Faremonster</h2>
                            <p>Faremonster is more than just a website or company.Faremonster is for belief of agents that every agents has for their travelers, to distinctive experience of their travel and to grow.</p>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con1"> <img src="img/contact/1.html" alt="">
                            <h4>Address</h4>
                            <p>
                                <br></p>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con3"> <img src="img/contact/2.html" alt="">
                            <h4>CONTACT INFO:</h4>
                            <p> <a href="tel://0099999999" class="contact-icon">Mobile: +91 00 0000 0000</a>
                                <br> <a href="mailto:mytestmail@gmail.com" class="contact-icon">Email: info@Faremonster.com</a> </p>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con4"> <img src="img/contact/3.html" alt="">
                            <h4>Website</h4>
                            <p> <a href="#">Website: www.Faremonster.in</a>
                            
                                </p>
                        </div>
                    </div>				
			</div>
		</div>
	</section>
    
      
    

</asp:Content>

